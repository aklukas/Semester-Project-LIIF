import fvcore.nn
import torch
import torch.nn
import datasets
import models
import utils
import argparse
import os
import yaml
import csv

def fixed_input_flops(model):
    #In dims:
    in_h = 256
    in_w = 256

    in_tensor = torch.rand([1,3,in_h,in_w]).cuda()

    #Can't use full amount since it requires too much cuda memory

    max_pixels_to_compute = 30000 
    #Based on the eval_bsize in the test scripts

    out_tensor = torch.rand([1,max_pixels_to_compute,2]).cuda()

    inp_full = (in_tensor,out_tensor,out_tensor)

    flops = fvcore.nn.FlopCountAnalysis(model, inp_full)

    #print("Flops:--------------------------------------------------------------------------")
    #print(fvcore.nn.flop_count_table(flops))
    #print("--------------------------------------------------------------------------------")

    encoder_flops = flops.total("encoder")
    decoder_flops_unscaled = flops.total("imnet.layers")

    flop_results = []
    
    for factor in range(1,5):
        HR_h = factor * in_h
        HR_w = factor * in_w

        pixels_to_compute = HR_h * HR_w

        scale_factor = pixels_to_compute/max_pixels_to_compute

        total_flops = encoder_flops + decoder_flops_unscaled*scale_factor

        flop_results.append(str(int(2*total_flops)))
    
    return flop_results

def fixed_output_flops(model):
    #Out dims: Used in most models
    HR_h = 1280
    HR_w = 720

    pixels_to_compute = HR_h*HR_w
    #Can't use full amount since it requires too much cuda memory

    max_pixels_to_compute = 30000 
    #Based on the eval_bsize in the test scripts
    scale_factor = (pixels_to_compute/max_pixels_to_compute)

    out_tensor = torch.rand([1,max_pixels_to_compute,2]).cuda()

    flop_results = []

    for factor in range(1,5):
        #In dims:
        LR_h = int(HR_h/factor)
        LR_w = int(HR_w/factor)

        LR_in = torch.rand([1,3,LR_h,LR_w]).cuda()

        inp_full = (LR_in,out_tensor,out_tensor)

        flops = fvcore.nn.FlopCountAnalysis(model, inp_full)

        encoder_flops = flops.total("encoder")
        decoder_flops_unscaled = flops.total("imnet.layers")

        total_flops = encoder_flops + decoder_flops_unscaled*scale_factor

        flop_results.append(str(int(2*total_flops)))
    
    return flop_results

def enc_dec_ratio(model):
    #In dims:
    in_h = 256
    in_w = 256

    in_tensor = torch.rand([1,3,in_h,in_w]).cuda()

    #Can't use full amount since it requires too much cuda memory

    max_pixels_to_compute = 30000 
    #Based on the eval_bsize in the test scripts

    out_tensor = torch.rand([1,max_pixels_to_compute,2]).cuda()

    inp_full = (in_tensor,out_tensor,out_tensor)

    flops = fvcore.nn.FlopCountAnalysis(model, inp_full)

    encoder_flops = flops.total("encoder")
    decoder_flops_unscaled = flops.total("imnet.layers")

    flop_results = []
    
    for factor in range(1,5):
        HR_h = factor * in_h
        HR_w = factor * in_w

        pixels_to_compute = HR_h * HR_w

        scale_factor = pixels_to_compute/max_pixels_to_compute

        ratio_flops = encoder_flops/(decoder_flops_unscaled*scale_factor)

        flop_results.append(str(ratio_flops))
    
    return flop_results

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    result_file = "FLOP_results.csv"

    header = ["Model","In X1","In X2","In X3","In X4","Out X1","Out X2","Out X3","Out X4","Ratio X1","Ratio X2","Ratio X3","Ratio X4"]

    
    with open(result_file, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)
        file.close()
    

    encoder_params = {
        "resblocks": [1,2,4,8,16],
        "feats": [4,8,16,32,64] 
    }

    decoder_params = {
        "hidden_layers": [1,2,3,4],
        "dims": [8,16,32,64,128,256] 
    }

    for resblocks in encoder_params['resblocks']:
        for feats in encoder_params['feats']:
            for dims in decoder_params['dims']:
                for hidden_layers in decoder_params['hidden_layers']:
                    # Fill hidden list
                    hidden_layers_filled = []
                    for x in range(hidden_layers):
                        hidden_layers_filled.append(dims)

                    # Build model
                    encoder_spec = {
                        "name" : "edsr",
                        "args" : {
                            "no_upsampling" : "true",
                            "n_feats": feats,
                            "n_resblocks": resblocks
                        }
                    }

                    immnet_spec = {
                        "name" : "mlp",
                        "args" : {
                            "out_dim" : 3,
                            "hidden_list" : hidden_layers_filled
                        }
                    }

                    print(immnet_spec)

                    model_spec = {
                        "name" : "liif",
                        "args" : {
                            "encoder_spec" : encoder_spec,
                            "imnet_spec" : immnet_spec
                        }
                    }

                    # Make model
                    model = models.make(model_spec).cuda()

                    model.eval()

                    torch.no_grad()

                    # Name of model:
                    name = "EDSR-Enc_" + str(resblocks) + "_resblock_" + str(feats) + "_feats-Dec_"
                    name += str(hidden_layers) + "_Hidden_layers_" + str(dims) + "_Dims" 

                    print(name)

                    
                    const_input_flops = fixed_input_flops(model)
                    const_output_flops = fixed_output_flops(model)
                    ratio_flops = enc_dec_ratio(model)

                    csv_row = []
                    csv_row.append(name)
                    for val in const_input_flops:
                        csv_row.append(val)
                    for val in const_output_flops:
                        csv_row.append(val)
                    for val in ratio_flops:
                        csv_row.append(val)

                    print(csv_row)
                    
                    # Add to results file: 
                    
                    with open(result_file, 'a') as file:
                        writer = csv.writer(file)
                        writer.writerow(csv_row)
                        file.close()
                    

