echo 'set5' &&
echo 'x2' &&
python test.py --config ./configs/test/test-set5-2_SSIM.yaml --model $1 --gpu $2 &&
echo 'x3' &&
python test.py --config ./configs/test/test-set5-3_SSIM.yaml --model $1 --gpu $2 &&
echo 'x4' &&
python test.py --config ./configs/test/test-set5-4_SSIM.yaml --model $1 --gpu $2 &&

echo 'set14' &&
echo 'x2' &&
python test.py --config ./configs/test/test-set14-2_SSIM.yaml --model $1 --gpu $2 &&
echo 'x3' &&
python test.py --config ./configs/test/test-set14-3_SSIM.yaml --model $1 --gpu $2 &&
echo 'x4' &&
python test.py --config ./configs/test/test-set14-4_SSIM.yaml --model $1 --gpu $2 &&

echo 'b100' &&
echo 'x2' &&
python test.py --config ./configs/test/test-b100-2_SSIM.yaml --model $1 --gpu $2 &&
echo 'x3' &&
python test.py --config ./configs/test/test-b100-3_SSIM.yaml --model $1 --gpu $2 &&
echo 'x4' &&
python test.py --config ./configs/test/test-b100-4_SSIM.yaml --model $1 --gpu $2 &&

echo 'urban100' &&
echo 'x2' &&
python test.py --config ./configs/test/test-urban100-2_SSIM.yaml --model $1 --gpu $2 &&
echo 'x3' &&
python test.py --config ./configs/test/test-urban100-3_SSIM.yaml --model $1 --gpu $2 &&
echo 'x4' &&
python test.py --config ./configs/test/test-urban100-4_SSIM.yaml --model $1 --gpu $2 &&

true
