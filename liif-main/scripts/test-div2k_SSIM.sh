echo 'div2k-x2' &&
python test.py --config ./configs/test/test-div2k-2_SSIM.yaml --model $1 --gpu $2 &&
echo 'div2k-x3' &&
python test.py --config ./configs/test/test-div2k-3_SSIM.yaml --model $1 --gpu $2 &&
echo 'div2k-x4' &&
python test.py --config ./configs/test/test-div2k-4_SSIM.yaml --model $1 --gpu $2 &&

true
