echo 'div2k-x2' &&
python demo.py --input ./load/div2k/DIV2K_valid_LR_bicubic/X2/0801x2.png --model $1 --resolution 1356,2040 --output demo_out_images/0801x2_out.png --gpu $2 &&
echo 'div2k-x3' &&
python demo.py --input ./load/div2k/DIV2K_valid_LR_bicubic/X3/0801x3.png --model $1 --resolution 1356,2040 --output demo_out_images/0801x3_out.png --gpu $2 &&
echo 'div2k-x4' &&
python demo.py --input ./load/div2k/DIV2K_valid_LR_bicubic/X4/0801x4.png --model $1 --resolution 1356,2040 --output demo_out_images/0801x4_out.png --gpu $2 &&

true
