import os
import re
import operator

def extract_params_from_model(model_folder):
    params = []

    #Extract Resblocks
    temp = re.search('Enc_(.+?)_resblock', entry)
    if temp:
        params.append(int(temp.group(1)))

    #Extract nbr feats
    temp = re.search('resblock_(.+?)_feats', entry)
    if temp:
        params.append(int(temp.group(1)))

    #Extract nbr hidden layers
    temp = re.search('Dec_(.+?)_Hidden', entry)
    if temp:
        params.append(int(temp.group(1)))
    
    #Extract hidden weights
    temp = re.search('layers_(.+?)_Dims', entry) 
    if temp:
        params.append(int(temp.group(1)))
    
    return params

if __name__ == '__main__':
    list = os.listdir("./save")
    list = sorted(list)

    completed_file = "Completed_Models_Scanned.txt"

    all_found_model_params = []

    for entry in list:
        model = os.path.join("./save",entry, "epoch-1000.pth")
        if os.path.exists(model):
            params = extract_params_from_model(entry)
            all_found_model_params.append(params)
    
    # Sort all found models by params in order: Res, Feat, Layers, Dims
    sorted = sorted(all_found_model_params, key = lambda x: (int(x[0]),int(x[1]),int(x[2]),int(x[3])))

    with open(completed_file, "w") as f:
        for param in sorted:
            print(param, ",", file=f)


