import fvcore.nn
import torch
import torch.nn
import datasets
import models
import utils
import argparse
import os

#"edsr-baseline", "edsr", "rdn"
encoder_spec = {
    "name" : "edsr",
    "args" : {
        "no_upsampling" : "true",
        "n_feats": 64
    }
}

immnet_spec = {
      "name" : "mlp",
      "args" : {
        "out_dim" : 3,
        "hidden_list" : [256, 256, 256, 256]
      }
}

model_spec = {
    "name" : "liif",
    "args" : {
        "encoder_spec" : encoder_spec,
        "imnet_spec" : immnet_spec
    }
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    parser.add_argument('--in_dims', action='append')
    parser.add_argument("--out_dims", action='append')
    args = parser.parse_args()

    in_h = parser.in_dims[0]
    in_w = parser.in_dims[1]

    out_h = parser.out_dims[0]
    out_w = parser.out_dims[1]

    pixels_to_compute = out_h*out_w

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    model = models.make(model_spec).cuda()

    model.eval()

    torch.no_grad()

    inp_full = (torch.rand([1,3,in_h,in_w]).cuda(),torch.rand([1,pixels_to_compute,2]).cuda(),torch.rand([1,pixels_to_compute,2]).cuda())
    inp_enc = torch.rand([1,3,in_h,in_w]).cuda()

    flops = fvcore.nn.FlopCountAnalysis(model, inp_full)

    print("These are MACs for FLOPs take the double of these values")
    print(fvcore.nn.flop_count_table(flops))