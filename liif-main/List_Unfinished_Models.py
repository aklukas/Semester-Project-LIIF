import os
import re
import operator

def extract_params_from_model(model_folder):
    params = []

    #Extract Resblocks
    temp = re.search('Enc_(.+?)_resblock', entry)
    if temp:
        params.append(int(temp.group(1)))

    #Extract nbr feats
    temp = re.search('resblock_(.+?)_feats', entry)
    if temp:
        params.append(int(temp.group(1)))

    #Extract nbr hidden layers
    temp = re.search('Dec_(.+?)_Hidden', entry)
    if temp:
        params.append(int(temp.group(1)))
    
    #Extract hidden weights
    temp = re.search('layers_(.+?)_Dims', entry) 
    if temp:
        params.append(int(temp.group(1)))
    
    return params

if __name__ == '__main__':
    dir_list = os.listdir("./save")
    dir_list = sorted(dir_list)

    completed_file = "Incomplete_Models.txt"

    with open(completed_file, "w") as f:
        f.close()

    all_found_model_params = []

    for entry in dir_list:
        model = os.path.join("./save",entry, "epoch-1000.pth")
        if not os.path.exists(model):
            params = extract_params_from_model(entry)
            if len(params):
                all_found_model_params.append(params)
    
    if len(all_found_model_params):
        sorted = sorted(all_found_model_params, key = lambda x: (int(x[0]),int(x[1]),int(x[2]),int(x[3])))

        with open(completed_file, "w") as f:
            for param in sorted:
                # Name of save:
                name = "EDSR-Enc_" + str(param[0]) + "_resblock_" + str(param[1]) + "_feats-Dec_"
                name += str(param[2]) + "_Hidden_layers_" + str(param[3]) + "_Dims" 
                print(name, file=f)
                f.close()


