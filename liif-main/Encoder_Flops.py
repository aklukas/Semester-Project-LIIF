import fvcore.nn
import torch
import torch.nn
import datasets
import models
import utils
import argparse
import os


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    encoder_spec = {
    "name" : "",
    "args" : {
        "no_upsampling" : "true",
        }
    }

    in_h = 256
    in_w = 256

    inp = torch.rand([1,3,in_h,in_w]).cuda()

    encoders = ['edsr-baseline', 'edsr', 'rdn']

    for enc in encoders:
        encoder_spec["name"] = enc
        model = models.make(encoder_spec).cuda()
        model.eval()
        torch.no_grad()
        flops = fvcore.nn.FlopCountAnalysis(model, inp)
        print(enc)
        print(2*flops.total())

    # Results
    """ 
    edsr-baseline:  79'838'576'640
    edsr:         2513'008'852'992
    rdn:          1439'464'161'280
    These are the Results in MACs so it's double for FLOPs
    """
        