import torch
import fvcore.nn
import torch
import torch.nn
import argparse
import os
import yaml
import csv
from swinir import SwinIR

#FLOPs of the model aren't linear to input image size: Ex: for 10 x 10 input it's 5M Flops, 
# for 256 x 256 2M Flops per pixel and the paper says for 640 x 360 it's 1.69 M Flops/pixel

def fixed_input_flops(model):
    #In dims:
    in_h = 256 
    in_w = 256

    input = torch.rand([1,3,in_h,in_w])

    flops = fvcore.nn.FlopCountAnalysis(model, input)

    #print("Flops:--------------------------------------------------------------------------")
    #print(fvcore.nn.flop_count_table(flops))
    #print("--------------------------------------------------------------------------------")
    #print(flops.total())
    return 2*flops.total()

def fixed_output_flops(model,scale):
    #Out dims:
    HR_h = 640 #1280
    HR_w = 360 #720 #had to halve them as CUDA runs out of memory otherwise

    #In dims:
    LR_h = int(HR_h/scale)
    LR_w = int(HR_w/scale)

    input = torch.rand([1,3,LR_h,LR_w])

    flops = fvcore.nn.FlopCountAnalysis(model, input)

    #print("Flops:--------------------------------------------------------------------------")
    #print(fvcore.nn.flop_count_table(flops))
    #print("--------------------------------------------------------------------------------")
    #print(flops.total())
    return 4 * 2*flops.total()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    result_file = "SWINIR_FLOPs.csv"

    header = ["Model","In X2","In X3","In X4","Out X2","Out X3","Out X4"]

    
    with open(result_file, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)
        file.close()

    # Models to test:
    classical_sr = {
        "Name" : "classical_sr",
        "depths" : [6, 6, 6, 6, 6, 6],
        "num_heads" : [6, 6, 6, 6, 6, 6],
        "embed_dim" : 180,
        "mlp_ratio" : 2,
        "upsampler" : "pixelshuffle",
        #"upsampler" : "pixelshuffledirect",
        "resi_connection" : "1conv",
        "img_range" : 1,
        "in_chans" : 3,
        "img_size" : 64,
        "window_size" : 8
    }

    light_sr = {
        "Name" : "light_sr",
        "depths" : [6, 6, 6, 6],
        "num_heads" : [6, 6, 6, 6],
        "embed_dim" : 60,
        "mlp_ratio" : 2,
        "upsampler" : "pixelshuffledirect",
        "resi_connection" : "1conv",
        "img_range" : 1,
        "in_chans" : 3,
        "img_size" : 64,
        "window_size" : 8
    }

    real_sr = {
        "Name" : "real_sr",
        "depths" : [6, 6, 6, 6, 6, 6],
        "num_heads" : [6, 6, 6, 6, 6, 6],
        "embed_dim" : 180,
        "mlp_ratio" : 2,
        "upsampler" : "nearest+conv",
        "resi_connection" : "1conv",
        "img_range" : 1,
        "in_chans" : 3,
        "img_size" : 64,
        "window_size" : 8
    }

    model_configs = [classical_sr,light_sr,real_sr]

    #print(model.flops()) same as if i ran fvcore with a 64 x 64 input

    for model_conf in model_configs:
        flop_results_in = []
        flop_results_out = []
        for factor in range(2,5):
            model = SwinIR(upscale=factor, in_chans=3, img_size=64, window_size=8,
                        img_range=1., depths=model_conf["depths"], embed_dim=model_conf["embed_dim"],
                        num_heads=model_conf["num_heads"], mlp_ratio=2, upsampler=model_conf["upsampler"],
                        resi_connection=model_conf["resi_connection"])
            model.eval()
            torch.no_grad()
            flops = fixed_input_flops(model)
            flop_results_in.append(str(int(flops)))
            flops = fixed_output_flops(model,factor)
            flop_results_out.append(str(int(flops)))
            #print("Completed a scale")

        csv_row = []
        csv_row.append(model_conf["Name"])
        for val in flop_results_in:
            csv_row.append(val)
        for val in flop_results_out:
            csv_row.append(val)

        print(csv_row)
                        
        # Add to results file: 
        with open(result_file, 'a') as file:
            writer = csv.writer(file)
            writer.writerow(csv_row)
            file.close()