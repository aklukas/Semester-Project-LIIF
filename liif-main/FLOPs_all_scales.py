import fvcore.nn
import torch
import torch.nn
import datasets
import models
import utils
import argparse
import os

#"edsr-baseline", "edsr", "rdn"
encoder_spec = {
    "name" : "edsr",
    "args" : {
        "no_upsampling" : "true",
        "n_feats": 8,
        "n_resblocks": 4
    }
}

immnet_spec = {
      "name" : "mlp",
      "args" : {
        "out_dim" : 3,
        "hidden_list" : [32]
      }
}

model_spec = {
    "name" : "liif",
    "args" : {
        "encoder_spec" : encoder_spec,
        "imnet_spec" : immnet_spec
    }
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    #Out dims:
    HR_h = 1020
    HR_w = 1020

    pixels_to_compute = HR_h*HR_w
    #Can't use full amount since it requires too much cuda memory

    max_pixels_to_compute = 30000 
    #Based on the eval_bsize in the test scripts

    print("Batches to fit output image: ")
    print(pixels_to_compute/max_pixels_to_compute)

    out_tensor = torch.rand([1,max_pixels_to_compute,2]).cuda()

    #In dims:
    x1_h = HR_h
    x1_w = HR_w

    x2_h = int(HR_h/2)
    x2_w = int(HR_w/2)

    x3_h = int(HR_h/3)
    x3_w = int(HR_w/3)

    x4_h = int(HR_h/4)
    x4_w = int(HR_w/4)

    x1_in = torch.rand([1,3,x1_h,x1_w]).cuda()
    x2_in = torch.rand([1,3,x2_h,x2_w]).cuda()
    x3_in = torch.rand([1,3,x3_h,x3_w]).cuda()
    x4_in = torch.rand([1,3,x4_h,x4_w]).cuda()

    #Create model to evaluate
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    model = models.make(model_spec).cuda()

    model.eval()

    torch.no_grad()

    inp_full_x1 = (x1_in,out_tensor,out_tensor)
    inp_full_x2 = (x2_in,out_tensor,out_tensor)
    inp_full_x3 = (x3_in,out_tensor,out_tensor)
    inp_full_x4 = (x4_in,out_tensor,out_tensor)

    flops_x1 = fvcore.nn.FlopCountAnalysis(model, inp_full_x1)
    flops_x2 = fvcore.nn.FlopCountAnalysis(model, inp_full_x2)
    flops_x3 = fvcore.nn.FlopCountAnalysis(model, inp_full_x3)
    flops_x4 = fvcore.nn.FlopCountAnalysis(model, inp_full_x4)

    print("X1 Flops:-----------------------------------------------------------------------")
    print(fvcore.nn.flop_count_table(flops_x1))
    print("--------------------------------------------------------------------------------")

    print("X2 Flops:-----------------------------------------------------------------------")
    print(fvcore.nn.flop_count_table(flops_x2))
    print("--------------------------------------------------------------------------------")

    print("X3 Flops:-----------------------------------------------------------------------")
    print(fvcore.nn.flop_count_table(flops_x3))
    print("--------------------------------------------------------------------------------")

    print("X4 Flops:-----------------------------------------------------------------------")
    print(fvcore.nn.flop_count_table(flops_x4))
    print("--------------------------------------------------------------------------------")

    print("These are actually all MACs as such the FLOPs are double these values")