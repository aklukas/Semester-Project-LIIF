import argparse
import os
import math
from functools import partial
import re

import yaml
from tqdm import tqdm

import datasets
import models
import utils

import warnings
import csv

from skimage.metrics import structural_similarity as ssim
from skimage.color import rgb2gray

warnings.filterwarnings("ignore",category=DeprecationWarning)
warnings.filterwarnings("ignore",category=PendingDeprecationWarning)
warnings.filterwarnings("ignore",category=UserWarning)
import torch
from torch.utils.data import DataLoader


def batched_predict(model, inp, coord, cell, bsize):
    with torch.no_grad():
        model.gen_feat(inp)
        n = coord.shape[1]
        ql = 0
        preds = []
        while ql < n:
            qr = min(ql + bsize, n)
            pred = model.query_rgb(coord[:, ql: qr, :], cell[:, ql: qr, :])
            preds.append(pred)
            ql = qr
        pred = torch.cat(preds, dim=1)
    return pred


def eval_psnr(loader, model, data_norm=None, eval_type=None, eval_bsize=None,
              verbose=False, use_ssim=False):
    model.eval()

    if data_norm is None:
        data_norm = {
            'inp': {'sub': [0], 'div': [1]},
            'gt': {'sub': [0], 'div': [1]}
        }
    t = data_norm['inp']
    inp_sub = torch.FloatTensor(t['sub']).view(1, -1, 1, 1).cuda()
    inp_div = torch.FloatTensor(t['div']).view(1, -1, 1, 1).cuda()
    t = data_norm['gt']
    gt_sub = torch.FloatTensor(t['sub']).view(1, 1, -1).cuda()
    gt_div = torch.FloatTensor(t['div']).view(1, 1, -1).cuda()

    if eval_type is None:
        metric_fn = utils.calc_psnr
    elif eval_type.startswith('div2k'):
        scale = int(eval_type.split('-')[1])
        metric_fn = partial(utils.calc_psnr, dataset='div2k', scale=scale)
    elif eval_type.startswith('benchmark'):
        scale = int(eval_type.split('-')[1])
        metric_fn = partial(utils.calc_psnr, dataset='benchmark', scale=scale)
    else:
        raise NotImplementedError

    val_res = utils.Averager()

    pbar = tqdm(loader, leave=False, desc='val')
    for batch in pbar:
        for k, v in batch.items():
            batch[k] = v.cuda()

        #print(batch['inp'].size())    

        inp = (batch['inp'] - inp_sub) / inp_div
        #print(inp.size())
        #print(batch['coord'].size())
        if eval_bsize is None:
            with torch.no_grad():
                pred = model(inp, batch['coord'], batch['cell'])
        else:
            pred = batched_predict(model, inp,
                batch['coord'], batch['cell'], eval_bsize)
        pred = pred * gt_div + gt_sub
        pred.clamp_(0, 1)

        if eval_type is not None: # reshape for shaving-eval
            ih, iw = batch['inp'].shape[-2:]
            s = math.sqrt(batch['coord'].shape[1] / (ih * iw))
            shape = [batch['inp'].shape[0], round(ih * s), round(iw * s), 3]
            pred = pred.view(*shape) \
                .permute(0, 3, 1, 2).contiguous()
            batch['gt'] = batch['gt'].view(*shape) \
                .permute(0, 3, 1, 2).contiguous()

        #Batch and pred are both size: 1x3xWxH
        """
        Convert to ndarray or right size: HxWx3
        test = torch.squeeze(pred)
        print(test.size())
        test = test.permute(2,1,0)
        print(test.size())
        test = test.cpu().numpy()
        print(type(test))
        """
        if use_ssim:
            # Convert from 1xCxWxH to HxWxC for rgb2gray and then ssim
            pred = pred.squeeze().permute(2,1,0).cpu().numpy()
            batch['gt'] = batch['gt'].squeeze().permute(2,1,0).cpu().numpy()
            ssim_result = ssim(rgb2gray(pred), rgb2gray(batch['gt']),data_range=1.0)
            val_res.add(ssim_result,1)
        else:
            res = metric_fn(pred, batch['gt'])
            val_res.add(res.item(), inp.shape[0])

        if verbose:
            pbar.set_description('val {:.4f}'.format(val_res.item()))

    return val_res.item()


def eval_image(loader, model, data_norm=None, eval_bsize=None):
    model.eval()

    if data_norm is None:
        data_norm = {
            'inp': {'sub': [0], 'div': [1]},
            'gt': {'sub': [0], 'div': [1]}
        }
    t = data_norm['inp']
    inp_sub = torch.FloatTensor(t['sub']).view(1, -1, 1, 1).cuda()
    inp_div = torch.FloatTensor(t['div']).view(1, -1, 1, 1).cuda()
    t = data_norm['gt']
    gt_sub = torch.FloatTensor(t['sub']).view(1, 1, -1).cuda()
    gt_div = torch.FloatTensor(t['div']).view(1, 1, -1).cuda()

    pbar = tqdm(loader, leave=False, desc='val')
    for batch in pbar:
        for k, v in batch.items():
            batch[k] = v.cuda()

        inp = (batch['inp'] - inp_sub) / inp_div
        if eval_bsize is None:
            with torch.no_grad():
                pred = model(inp, batch['coord'], batch['cell'])
        else:
            pred = batched_predict(model, inp,
                batch['coord'], batch['cell'], eval_bsize)
        pred = pred * gt_div + gt_sub
        pred.clamp_(0, 1)

       # reshape for shaving-eval
        ih, iw = batch['inp'].shape[-2:]
        s = math.sqrt(batch['coord'].shape[1] / (ih * iw))
        shape = [batch['inp'].shape[0], round(ih * s), round(iw * s), 3]
        pred = pred.view(*shape) \
            .permute(0, 3, 1, 2).contiguous()
        batch['gt'] = batch['gt'].view(*shape) \
            .permute(0, 3, 1, 2).contiguous()

        #Batch and pred are both size: 1x3xWxH
        """
        Convert to ndarray or right size: HxWx3
        test = torch.squeeze(pred)
        print(test.size())
        test = test.permute(2,1,0)
        print(test.size())
        test = test.cpu().numpy()
        print(type(test))
        """

        pred = pred[0].squeeze().permute(0,2,1).cpu()

    return pred

def test_func(loader, config, model):
    res = eval_psnr(loader, model,
        data_norm=config.get('data_norm'),
        eval_type=config.get('eval_type'),
        eval_bsize=config.get('eval_bsize'),
        verbose=True, use_ssim=config.get("use_ssim"))
    return res

def extract_params_from_model(entry):
    params = {
        "resblocks": None,
        "feats": None, 
        "hidden_layers": None,
        "dims": None 
    }

    #Extract Resblocks
    temp = re.search('Enc_(.+?)_resblock', entry)
    if temp:
        params["resblocks"] = temp.group(1)
    temp = re.search('resblock_(.+?)_feats', entry)

    #Extract nbr feats
    if temp:
        params["feats"] = temp.group(1)
    temp = re.search('Dec_(.+?)_Hidden', entry)

    #Extract nbr hidden layers
    if temp:
        params["hidden_layers"] = temp.group(1)
    temp = re.search('layers_(.+?)_Dims', entry)

    #Extract hidden weights
    if temp:
        params["dims"] = temp.group(1)
    
    return params


def test_dataset(configs_to_test, result_file, model_folder):
    loaders = []
    configs = []

    header = ["Model","resblocks", "feats", "hidden layers", "dims", "PSNR X2","PSNR X3","PSNR X4","SSIM X2","SSIM X3","SSIM X4"]

    with open(result_file, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)
        file.close()

    # Build all data loaders
    for config_path in configs_to_test: 
        with open(config_path, 'r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
            configs.append(config)

        spec = config['test_dataset']
        dataset = datasets.make(spec['dataset'])
        dataset = datasets.make(spec['wrapper'], args={'dataset': dataset})
        loader = DataLoader(dataset, batch_size=spec['batch_size'],
            num_workers=8, pin_memory=True)
        loaders.append(loader)
    print("DATA Loaders Built")

    #Parse the model folder and grab a model to test
    model_folder_list = os.listdir(model_folder)
    model_folder_list.sort()
    for entry in model_folder_list:
        model_path = os.path.join(model_folder,entry, "epoch-last.pth")
        model_completed_path = os.path.join(model_folder,entry, "epoch-1000.pth")
        if os.path.exists(model_path) and os.path.exists(model_completed_path):
            #Valid model with last epoch completed
            #Load model then parse all datasets for that model
            model_path_relative = os.path.relpath(model_path,".")
            print("Building model:")
            print(model_path_relative)
            model_spec = torch.load(model_path_relative)['model']
            model = models.make(model_spec, load_sd=True).cuda()
            print("Testing model:")
            print(model_path)
            results = []    
            results.append(entry)

            params = extract_params_from_model(entry)
            for param in params:
                results.append(params[param])

            for loader, config, config_path in zip(loaders, configs, configs_to_test):
                print("Testing config:")
                print(config_path)
                results.append(str(test_func(loader, config, model)))
                
            # Add to results file: 
            with open(result_file, 'a') as file:
                writer = csv.writer(file)
                writer.writerow(results)
                file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_folder')
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    configs_to_test_Set5 = ["./configs/test/test-set5-2.yaml",
                        "./configs/test/test-set5-3.yaml",
                        "./configs/test/test-set5-4.yaml",
                        "./configs/test/test-set5-2_SSIM.yaml",
                        "./configs/test/test-set5-3_SSIM.yaml",
                        "./configs/test/test-set5-4_SSIM.yaml"]

    configs_to_test_Set14 = ["./configs/test/test-set14-2.yaml",
                        "./configs/test/test-set14-3.yaml",
                        "./configs/test/test-set14-4.yaml",
                        "./configs/test/test-set14-2_SSIM.yaml",
                        "./configs/test/test-set14-3_SSIM.yaml",
                        "./configs/test/test-set14-4_SSIM.yaml"]                    
    
    configs_to_test_B100 = ["./configs/test/test-b100-2.yaml",
                        "./configs/test/test-b100-3.yaml",
                        "./configs/test/test-b100-4.yaml",
                        "./configs/test/test-b100-2_SSIM.yaml",
                        "./configs/test/test-b100-3_SSIM.yaml",
                        "./configs/test/test-b100-4_SSIM.yaml"]   

    configs_to_test_Urban100 = ["./configs/test/test-urban100-2.yaml",
                        "./configs/test/test-urban100-3.yaml",
                        "./configs/test/test-urban100-4.yaml",
                        "./configs/test/test-urban100-2_SSIM.yaml",
                        "./configs/test/test-urban100-3_SSIM.yaml",
                        "./configs/test/test-urban100-4_SSIM.yaml"] 

    # Result files:
    result_file_set5 = "PSNR_SSIM_results_Set5.csv"
    result_file_set14 = "PSNR_SSIM_results_Set14.csv"
    result_file_b100 = "PSNR_SSIM_results_B100.csv"
    result_file_urban100 = "PSNR_SSIM_results_Urban100.csv"

                    
    test_dataset(configs_to_test_Set5,result_file_set5, args.model_folder)
    test_dataset(configs_to_test_Set14,result_file_set14, args.model_folder)
    test_dataset(configs_to_test_B100,result_file_b100, args.model_folder)
    test_dataset(configs_to_test_Urban100,result_file_urban100, args.model_folder)