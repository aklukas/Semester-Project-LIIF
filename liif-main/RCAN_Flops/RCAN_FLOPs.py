import sys
sys.path.append('../models')

from rcan import make_rcan
import torch
import fvcore.nn
import torch
import torch.nn
import models
import argparse
import os
import yaml
import csv

def fixed_input_flops(model):
    #In dims:
    in_h = 256
    in_w = 256

    input = torch.rand([1,3,in_h,in_w]).cuda()

    flops = fvcore.nn.FlopCountAnalysis(model, input)

    #print("Flops:--------------------------------------------------------------------------")
    #print(fvcore.nn.flop_count_table(flops))
    #print("--------------------------------------------------------------------------------")
    #print(flops.total())
    return 2*flops.total()

def fixed_output_flops(model,scale):
    #Out dims: 
    HR_h = 640 #1280
    HR_w = 360 #720 had to halve them as CUDA runs out of memory otherwise

    #In dims:
    LR_h = int(HR_h/scale)
    LR_w = int(HR_w/scale)

    input = torch.rand([1,3,LR_h,LR_w]).cuda()

    flops = fvcore.nn.FlopCountAnalysis(model, input)

    #print("Flops:--------------------------------------------------------------------------")
    #print(fvcore.nn.flop_count_table(flops))
    #print("--------------------------------------------------------------------------------")
    #print(flops.total())
    return 4 * 2*flops.total() # x4 cause output is x4 smaller than 1280 x 720


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    result_file = "RCAN_FLOPs.csv"

    header = ["RCAN","In X2","In X3","In X4","Out X2","Out X3","Out X4"]

    
    with open(result_file, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)
        file.close()

    flop_results_in = []
    flop_results_out = []

    for factor in range(2,5):
        model = make_rcan(scale=factor).cuda()
        model.eval()
        torch.no_grad()
        flops = fixed_input_flops(model)
        flop_results_in.append(str(int(flops)))
        flops = fixed_output_flops(model,factor)
        flop_results_out.append(str(int(flops)))
        print("Completed a scale")
    
    csv_row = []
    csv_row.append("RCAN")
    for val in flop_results_in:
        csv_row.append(val)
    for val in flop_results_out:
        csv_row.append(val)

    print(csv_row)
                    
    # Add to results file: 
    with open(result_file, 'a') as file:
        writer = csv.writer(file)
        writer.writerow(csv_row)
        file.close()