import yaml
import subprocess

# Temporary hard coding of already completed runs
already_done = [
    [1, 4, 1, 8] ,
[1, 4, 1, 16] ,
[1, 4, 1, 32] ,
[1, 4, 1, 64] ,
[1, 4, 1, 128] ,
[1, 4, 2, 8] ,
[1, 4, 2, 16] ,
[1, 4, 2, 32] ,
[1, 4, 2, 64] ,
[1, 4, 2, 128] ,
[1, 4, 3, 8] ,
[1, 4, 3, 16] ,
[1, 4, 3, 32] ,
[1, 4, 3, 64] ,
[1, 4, 3, 128] ,
[1, 4, 3, 256] ,
[1, 4, 4, 8] ,
[1, 4, 4, 16] ,
[1, 4, 4, 32] ,
[1, 4, 4, 64] ,
[1, 4, 4, 128] ,
[1, 4, 4, 256] ,
[1, 8, 1, 8] ,
[1, 8, 1, 16] ,
[1, 8, 1, 32] ,
[1, 8, 1, 64] ,
[1, 8, 1, 128] ,
[1, 8, 2, 8] ,
[1, 8, 2, 16] ,
[1, 8, 2, 32] ,
[1, 8, 2, 64] ,
[1, 8, 2, 128] ,
[1, 8, 3, 8] ,
[1, 8, 3, 16] ,
[1, 8, 3, 32] ,
[1, 8, 3, 64] ,
[1, 8, 3, 128] ,
[1, 8, 4, 8] ,
[1, 8, 4, 16] ,
[1, 8, 4, 32] ,
[1, 8, 4, 64] ,
[1, 8, 4, 128] ,
[1, 16, 1, 8] ,
[1, 16, 1, 16] ,
[1, 16, 1, 32] ,
[1, 16, 1, 64] ,
[1, 16, 1, 128] ,
[1, 16, 2, 8] ,
[1, 16, 2, 16] ,
[1, 16, 2, 32] ,
[1, 16, 2, 64] ,
[1, 16, 2, 128] ,
[1, 16, 3, 8] ,
[1, 16, 3, 16] ,
[1, 16, 3, 32] ,
[1, 16, 3, 64] ,
[1, 16, 3, 128] ,
[1, 16, 4, 8] ,
[1, 16, 4, 16] ,
[1, 16, 4, 32] ,
[1, 16, 4, 64] ,
[1, 16, 4, 128] ,
[1, 32, 1, 8] ,
[1, 32, 1, 16] ,
[1, 32, 1, 32] ,
[1, 32, 1, 64] ,
[1, 32, 1, 128] ,
[1, 32, 2, 8] ,
[1, 32, 2, 16] ,
[1, 32, 2, 32] ,
[1, 32, 2, 64] ,
[1, 32, 2, 128] ,
[1, 32, 3, 8] ,
[1, 32, 3, 16] ,
[1, 32, 3, 32] ,
[1, 32, 3, 64] ,
[1, 32, 3, 128] ,
[1, 32, 4, 8] ,
[1, 32, 4, 16] ,
[1, 32, 4, 32] ,
[1, 32, 4, 64] ,
[1, 32, 4, 128] ,
[1, 64, 1, 8] ,
[1, 64, 1, 16] ,
[1, 64, 1, 32] ,
[1, 64, 1, 64] ,
[1, 64, 1, 128] ,
[1, 64, 2, 8] ,
[1, 64, 2, 16] ,
[1, 64, 2, 32] ,
[1, 64, 2, 64] ,
[1, 64, 2, 128] ,
[1, 64, 3, 8] ,
[1, 64, 3, 16] ,
[1, 64, 3, 32] ,
[1, 64, 3, 64] ,
[1, 64, 3, 128] ,
[1, 64, 4, 8] ,
[1, 64, 4, 16] ,
[1, 64, 4, 32] ,
[1, 64, 4, 64] ,
[1, 64, 4, 128] ,
[2, 4, 1, 8] ,
[2, 4, 1, 16] ,
[2, 4, 1, 32] ,
[2, 4, 1, 64] ,
[2, 4, 1, 128] ,
[2, 4, 2, 8] ,
[2, 4, 2, 16] ,
[2, 4, 2, 32] ,
[2, 4, 2, 64] ,
[2, 4, 2, 128] ,
[2, 4, 3, 8] ,
[2, 4, 3, 16] ,
[2, 4, 3, 32] ,
[2, 4, 3, 64] ,
[2, 4, 3, 128] ,
[2, 4, 4, 8] ,
[2, 4, 4, 16] ,
[2, 4, 4, 32] ,
[2, 4, 4, 64] ,
[2, 4, 4, 128] ,
[2, 8, 1, 8] ,
[2, 8, 1, 16] ,
[2, 8, 1, 32] ,
[2, 8, 1, 64] ,
[2, 8, 2, 8] ,
[2, 8, 2, 16] ,
[2, 8, 2, 32] ,
[2, 8, 2, 64] ,
[2, 8, 3, 8] ,
[2, 8, 3, 16] ,
[2, 8, 3, 32] ,
[2, 8, 3, 64] ,
[2, 8, 3, 128] ,
[2, 8, 4, 8] ,
[2, 8, 4, 16] ,
[2, 8, 4, 32] ,
[2, 8, 4, 64] ,
[2, 8, 4, 128] ,
[2, 16, 3, 8] ,
[2, 16, 3, 16] ,
[2, 16, 3, 32] ,
[2, 16, 3, 64] ,
[2, 16, 3, 128] ,
[2, 16, 4, 8] ,
[2, 16, 4, 16] ,
[2, 16, 4, 32] ,
[2, 16, 4, 64] ,
[2, 16, 4, 128] ,
[2, 32, 3, 8] ,
[2, 32, 3, 16] ,
[2, 32, 3, 32] ,
[2, 32, 3, 64] ,
[2, 32, 3, 128] ,
[2, 32, 4, 8] ,
[2, 32, 4, 16] ,
[2, 32, 4, 32] ,
[2, 32, 4, 64] ,
[2, 32, 4, 128] ,
[2, 32, 4, 256] ,
[2, 64, 3, 8] ,
[2, 64, 3, 16] ,
[2, 64, 3, 32] ,
[2, 64, 3, 64] ,
[2, 64, 3, 128] ,
[2, 64, 4, 8] ,
[2, 64, 4, 16] ,
[2, 64, 4, 32] ,
[2, 64, 4, 64] ,
[2, 64, 4, 128] ,
[4, 4, 1, 8] ,
[4, 4, 1, 16] ,
[4, 4, 1, 32] ,
[4, 4, 1, 64] ,
[4, 4, 1, 128] ,
[4, 4, 2, 8] ,
[4, 4, 2, 16] ,
[4, 4, 2, 32] ,
[4, 4, 2, 64] ,
[4, 4, 2, 128] ,
[4, 4, 3, 8] ,
[4, 4, 3, 16] ,
[4, 4, 3, 32] ,
[4, 4, 3, 64] ,
[4, 4, 3, 128] ,
[4, 4, 4, 8] ,
[4, 4, 4, 16] ,
[4, 4, 4, 32] ,
[4, 4, 4, 64] ,
[4, 4, 4, 128] ,
[4, 8, 1, 8] ,
[4, 8, 1, 16] ,
[4, 8, 1, 32] ,
[4, 8, 1, 64] ,
[4, 8, 1, 128] ,
[4, 8, 2, 8] ,
[4, 8, 2, 16] ,
[4, 8, 2, 32] ,
[4, 8, 2, 64] ,
[4, 8, 2, 128] ,
[4, 8, 3, 8] ,
[4, 8, 3, 16] ,
[4, 8, 3, 32] ,
[4, 8, 3, 64] ,
[4, 8, 3, 128] ,
[4, 8, 4, 8] ,
[4, 8, 4, 16] ,
[4, 8, 4, 32] ,
[4, 8, 4, 64] ,
[4, 8, 4, 128] ,
[4, 16, 1, 8] ,
[4, 16, 1, 16] ,
[4, 16, 1, 32] ,
[4, 16, 1, 64] ,
[4, 16, 1, 128] ,
[4, 16, 2, 8] ,
[4, 16, 2, 16] ,
[4, 16, 2, 32] ,
[4, 16, 2, 64] ,
[4, 16, 2, 128] ,
[4, 16, 3, 8] ,
[4, 16, 3, 16] ,
[4, 16, 3, 32] ,
[4, 16, 3, 64] ,
[4, 16, 3, 128] ,
[4, 16, 4, 8] ,
[4, 16, 4, 16] ,
[4, 16, 4, 32] ,
[4, 16, 4, 64] ,
[4, 16, 4, 128] ,
[4, 32, 1, 8] ,
[4, 32, 1, 16] ,
[4, 32, 1, 32] ,
[4, 32, 1, 64] ,
[4, 32, 1, 128] ,
[4, 32, 2, 8] ,
[4, 32, 2, 16] ,
[4, 32, 2, 32] ,
[4, 32, 2, 64] ,
[4, 32, 2, 128] ,
[4, 32, 3, 8] ,
[4, 32, 3, 16] ,
[4, 32, 3, 32] ,
[4, 32, 3, 64] ,
[4, 32, 3, 128] ,
[4, 32, 4, 8] ,
[4, 32, 4, 16] ,
[4, 32, 4, 32] ,
[4, 32, 4, 64] ,
[4, 32, 4, 128] ,
[4, 64, 1, 8] ,
[4, 64, 1, 16] ,
[4, 64, 1, 32] ,
[4, 64, 1, 64] ,
[4, 64, 2, 8] ,
[4, 64, 2, 16] ,
[4, 64, 2, 32] ,
[4, 64, 2, 64] ,
[4, 64, 3, 8] ,
[4, 64, 3, 16] ,
[4, 64, 3, 32] ,
[4, 64, 3, 64] ,
[4, 64, 4, 8] ,
[4, 64, 4, 16] ,
[4, 64, 4, 32] ,
[4, 64, 4, 64] ,
[8, 16, 2, 64] ,
[8, 64, 4, 256] ,
[16, 32, 4, 128] ,
[16, 32, 4, 256] ,
[16, 64, 2, 128] ,
[16, 64, 3, 128] ,
[16, 64, 3, 256] ,
[16, 64, 4, 128]
]

def print_to_log(log_path, _res, _feat, _lay, _dim):
    with open(logger_file_path, 'a') as f:
        print("-Encoder:",file=f)
        print("--Res Blocks = " + str(_res),file=f)
        print("--Nbr Feats = " + str(_feat),file=f)
        print("-Decoder:",file=f)
        print("--Nbr Hidden layers = " + str(_lay),file=f)
        print("--Hidden dims = " + str(_dim),file=f)
        print(file=f)
        f.close()

def print_to_terminal(_res, _feat, _lay, _dim):
    print("-Encoder:")
    print("--Res Blocks = " + str(_res))
    print("--Nbr Feats = " + str(_feat))
    print("-Decoder:")
    print("--Nbr Hidden layers = " + str(_lay))
    print("--Hidden dims = " + str(_dim))
    print()


if __name__ == '__main__':

    logger_file_path = "configs/Param_Sweep_configs/param_sweep_logv1.txt"
    open(logger_file_path, 'w').close()

    encoder = {
        "resblocks": [1,2],
        "feats": [4,8,16,32,64] 
    }

    decoder = {
        "hidden_layers": [1,2],
        "dims": [8,16,32,64,128] 
    }

    base_file_path = "configs/Param_Sweep_configs/train_sweep_base_config.yaml"
    config_file_path = "configs/Param_Sweep_configs/train_sweep_configv1.yaml"

    with open(base_file_path, 'r') as file:
        base_config = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
    
    base_config_encoder = base_config["model"]["args"]["encoder_spec"]
    base_config_decoder = base_config["model"]["args"]["imnet_spec"]

    for resblocks in encoder['resblocks']:
        base_config_encoder['args']['n_resblocks'] = resblocks
        for feats in encoder['feats']:
            base_config_encoder['args']['n_feats'] = feats
            for dims in decoder['dims']:
                for hidden_layers in decoder['hidden_layers']:
                    # Fill hidden list
                    hidden_layers_filled = []
                    for x in range(hidden_layers):
                        hidden_layers_filled.append(dims)

                    base_config_decoder['args']['hidden_list'] = hidden_layers_filled 

                    # Fill config file
                    base_config["model"]["args"]["encoder_spec"] = base_config_encoder
                    base_config["model"]["args"]["imnet_spec"] = base_config_decoder

                    with open(config_file_path, 'w') as file:
                        yaml.dump(base_config, file)
                        file.close()
                    
                    # Name of save:
                    name = "EDSR-Enc_" + str(resblocks) + "_resblock_" + str(feats) + "_feats-Dec_"
                    name += str(hidden_layers) + "_Hidden_layers_" + str(dims) + "_Dims" 

                    blacklisted = False

                    current_group = [resblocks,feats,hidden_layers,dims]
                    for group in already_done:
                        if group == current_group:
                            blacklisted = True

                    # Call train function:
                    if blacklisted == False:
                        process = subprocess.run(["python3", "train_liif.py", "--config", config_file_path, "--gpu", "0", "--name", name])
                        print(process.check_returncode())
                        print("Return code:")
                        print(process.returncode)
                        if process.returncode == 0:
                            # Print to log
                            with open(logger_file_path, 'a') as f:
                                print("Finished Model with params:",file=f)
                                f.close()

                            # Print to terminal
                            print("Finished Model with params:")
                        else:
                            # Print to log
                            with open(logger_file_path, 'a') as f:
                                print("Run failed",file=f)
                                f.close()

                            # Print to terminal
                            print("Run failed")

                        # Print to log
                        print_to_log(logger_file_path,resblocks,feats,hidden_layers,dims)
                        # Print to terminal
                        print_to_terminal(resblocks,feats,hidden_layers,dims)
                    else:
                        # Print to log 
                        with open(logger_file_path, 'a') as f:
                                print("Skipping",file=f)
                                f.close()
                        print_to_log(logger_file_path,resblocks,feats,hidden_layers,dims)

                        # Print to terminal
                        print("Skipping")
                        print_to_terminal(resblocks,feats,hidden_layers,dims)

