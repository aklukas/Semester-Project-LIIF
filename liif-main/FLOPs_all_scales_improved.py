import fvcore.nn
import torch
import torch.nn
import datasets
import models
import utils
import argparse
import os

#"edsr-baseline", "edsr", "rdn"
encoder_spec = {
    "name" : "edsr",
    "args" : {
        "no_upsampling" : "true",
        "n_feats": 32,
        "n_resblocks": 4
    }
}

immnet_spec = {
      "name" : "mlp",
      "args" : {
        "out_dim" : 3,
        "hidden_list" : [64,64]
      }
}

model_spec = {
    "name" : "liif",
    "args" : {
        "encoder_spec" : encoder_spec,
        "imnet_spec" : immnet_spec
    }
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', default='0')
    args = parser.parse_args()

    #In dims:
    in_h = 256
    in_w = 256

    in_tensor = torch.rand([1,3,in_h,in_w]).cuda()

    #Out dims:
    HR_x2_h = 2*in_h
    HR_x2_w = 2*in_w
    HR_x3_h = 3*in_h
    HR_x3_w = 3*in_w
    HR_x4_h = 4*in_h
    HR_x4_w = 4*in_w

    pixels_to_compute_X2 = HR_x2_h*HR_x2_w
    pixels_to_compute_X3 = HR_x3_h*HR_x3_w
    pixels_to_compute_X4 = HR_x4_h*HR_x4_w
    #Can't use full amount since it requires too much cuda memory

    max_pixels_to_compute = 30000 
    #Based on the eval_bsize in the test scripts

    print("Batches to fit output image: ")
    print("X2:" + str(pixels_to_compute_X2/max_pixels_to_compute))
    print("X3:" + str(pixels_to_compute_X3/max_pixels_to_compute))
    print("X4:" + str(pixels_to_compute_X4/max_pixels_to_compute))

    out_tensor = torch.rand([1,max_pixels_to_compute,2]).cuda()

    #Create model to evaluate
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    model = models.make(model_spec).cuda()

    model.eval()

    torch.no_grad()

    # In tensor is the image for gen feat, out_tensor is given twice since query rgb needs the 
    # cell input and normal coord input (both same size)
    # See def of LIIF forward() function
    inp_full = (in_tensor,out_tensor,out_tensor)

    flops = fvcore.nn.FlopCountAnalysis(model, inp_full)

    print("Flops (Actually MACs):--------------------------------------------------------------------------")
    print(fvcore.nn.flop_count_table(flops))
    print("--------------------------------------------------------------------------------")

    print(2*flops.total("encoder"))
    print(2*flops.total("imnet.layers"))