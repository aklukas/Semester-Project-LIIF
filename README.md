# Semester Project LIIF

## Description
This repository contains the code used during my semester project studying the potential of LIIF models in light-weight image super resolution tasks. 

The project is heavily based on the LIIF model from: Yinbo Chen, Sifei Liu, Xiaolong Wang available at: https://github.com/yinboc/liif. 
Certain parts of their implementation were modified and further scripts were added to analyse how LIIF models could be made more light-weight. 

## Installation
See the LIIF README in the liif-main folder

Additionally the fvcore library needs to be installed for FLOP caculations.

## License
This project follows the same License as the original LIIF implementation it's based on
